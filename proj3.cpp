//
// Created by geoffreyc on 11/15/16.
//

#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/miller_rabin.hpp>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;
using namespace boost::multiprecision;

int main( int argc, char** argv )
{

    cpp_int p, q, e, d, N, c, x, encryption, decryption, phiN;
    string publicKeyFile, privateKeyFile, messageFile, encryptedFile,
            cipherFile, decryptedFile;
    ifstream fileIn;
    ofstream fileOut;

    cout << "Enter the file name that contains p, q, and e: ";
    cin >> publicKeyFile;
    cout << "Enter the file name to store d and N in: ";
    cin >> privateKeyFile;
    cout << "Enter the file name that contains x to be encrypted: ";
    cin >> messageFile;
    cout << "Enter the file name to store the E(x) in: ";
    cin >> encryptedFile;
    cout << "Enter the file name that contains the cipher text, c: ";
    cin >> cipherFile;
    cout << "Enter the file name to store the D(c) in: ";
    cin >> decryptedFile;

    fileIn.open( publicKeyFile.c_str( ) );
    if( !fileIn.is_open( ) )
    {
        cerr << "File not found." << endl;
        fileIn.close( );
        return 1;
    }
    else
    {
        fileIn >> p;
        fileIn >> q;
        fileIn >> e;
    }
    fileIn.close( );

    fileIn.open( messageFile.c_str( ) );
    if( !fileIn.is_open( ) )
    {
        cerr << "File not found." << endl;
        fileIn.close( );
        return 2;
    }
    else
    {
        fileIn >> x;
    }
    fileIn.close( );

    fileIn.open( cipherFile.c_str( ) );
    if( !fileIn.is_open( ) )
    {
        cerr << "File not found." << endl;
        fileIn.close( );
        return 3;
    }
    else
    {
        fileIn >> c;
    }
    fileIn.close( );

    N = p*q;

    // Finds the Euler's Totient of p and q
    phiN = ( p - 1 )*( q - 1 );

    //Finds the encryption of x
    encryption = powm( x, e, N );

    fileOut.open( encryptedFile.c_str( ) );
    fileOut << "E(" << x << ") is: " << encryption << endl;
    fileOut.close( );

    cpp_int m, n, l;
    m = 0;
    n = 1;
    l = phiN;
    cpp_int oldM, oldN, oldL;
    oldM = 1;
    oldN = 0;
    oldL = e;
    cpp_int quotient, temp;

    while( l != 0 )
    {
        quotient = oldL / l;
        temp = l;
        l = oldL - quotient*temp;
        oldL = temp;

        temp = m;
        m = oldM - quotient*temp;
        oldM = temp;

        temp = n;
        n = oldN - quotient*temp;
        oldN = temp;
    }

    // Check to make sure phiN and e are relative primes
    if( oldL != 1 )
    {
        cout << "phiN and e aren't relative primes" << endl;
        return 4;
    }
    // Finds the private key.
    d = abs( oldM );

    // Finds the decryption of c.
    decryption = powm( c, d, N );

    fileOut.open( decryptedFile.c_str( ) );
    fileOut << "D(" << c << ") is: " << decryption << endl;
    fileOut.close( );

    fileOut.open( privateKeyFile.c_str( ) );
    fileOut << "Private Key: " << d << endl;
    fileOut << "N: " << N << endl;
    fileOut.close( );

    return 0;
}
