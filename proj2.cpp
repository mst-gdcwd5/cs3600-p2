//
// Created by geoffreyc on 11/15/16.
//

#include <boost/multiprecision/cpp_int.hpp>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>

using namespace std;
using namespace boost::multiprecision;

cpp_int myGCD(cpp_int a, cpp_int b)
{
    cpp_int r;

    if(a < 0) a = -a;
    if(b < 0) b = -b;

    if(a == 0 && b == 0)
        r = -1;
    else if(b == 0)
        r = a;
    else
        r = myGCD(b, a % b);

    return r;
}

cpp_int myLinComb(cpp_int ia, cpp_int ib, cpp_int *x, cpp_int *y)
{
    cpp_int xnew, ynew;

    if (ia == 0)
    {
        *x = 0;
        *y = 1;
        return ib;
    }

    cpp_int g = myLinComb(ib%ia, ia, &xnew, &ynew);

    *x = ynew - (ib/ia) * xnew;
    *y = xnew;

    return 0;
}

int main() {

    cpp_int ia, ib;
    cpp_int x, y;
    char ix;
    string input, output;
    ifstream fileIn;
    ofstream fileOut;

    cout << "Enter the file name that contains a and b: ";
    cin >> input;
    cout << "Enter the file name to store the GCD and linear combination: ";
    cin >> output;


    fileIn.open( input.c_str( ) );
    if( !fileIn.is_open( ) )
    {
        cerr << "File not found." << endl;
        fileIn.close( );
        return 1;
    }
    else
    {
        fileIn >> ia;
        fileIn >> ib;
    }
    fileIn.close( );

    cpp_int g = myGCD(ia,ib);

    myLinComb(ia, ib, &x, &y);

    fileOut.open( output.c_str( ) );
    fileOut <<g <<endl <<x <<endl <<y << endl;
    fileOut.close( );


    return 0;
}