cmake_minimum_required(VERSION 3.6)
project(cs3600_p2)


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILESp2 proj2.cpp )
set(SOURCE_FILESp3 proj3.cpp )
add_executable(cs3600_p2 ${SOURCE_FILESp2})
add_executable(cs3600_p3 ${SOURCE_FILESp3})

find_package(Boost 1.36.0)
if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
    target_link_libraries(cs3600_p2 ${Boost_LIBRARIES})
    target_link_libraries(cs3600_p3 ${Boost_LIBRARIES})
endif()

file(COPY data/test.txt data/p.txt data/x.txt data/c.txt DESTINATION ${CMAKE_BINARY_DIR})